﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoboFarmer.Helpers
{
    public class OpenCVHelper
    {
        #region Singleton
        private static OpenCVHelper instance = null;
        private static readonly object padlock = new object();

        public static OpenCVHelper Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            instance = new OpenCVHelper();
                        }
                    }
                }
                return instance;
            }
        }
        #endregion


        Image<Bgr, Byte> screenshot = null;

        public void LoadScreenshot(string path)
        {
            screenshot = CvInvoke.Imread(path, ImreadModes.Color).ToImage<Bgr, Byte>();
        }

        public IList<Point> FindPattern(string patternPath)
        {
            Image<Bgr, Byte> pattern = CvInvoke.Imread(patternPath, ImreadModes.Color).ToImage<Bgr, Byte>();

            IList<Point> results = new List<Point>();

            using (Image<Gray, float> result = screenshot.MatchTemplate(pattern, TemplateMatchingType.CcoeffNormed))
            {
                for (int x = 0; x < result.Size.Width; x++)
                    for (int y = 0; y < result.Size.Height; y++)
                    {
                        if (result.Data[y, x, 0] > 0.99)
                        {
                            int center_x = x + pattern.Size.Width / 2;
                            int center_y = y + pattern.Size.Height / 2;

                            results.Add(new Point(center_x, center_y));
                        }
                    }
            }

            var preview = screenshot.Clone();

            foreach (var result in results)
            {
                preview.Draw(new CircleF(result, 10.0F), new Bgr(Color.Red), 1);
            }

            preview.Save("preview.png");

            return results;
        }
    }
}

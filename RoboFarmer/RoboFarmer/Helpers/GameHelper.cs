﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RoboFarmer.Helpers
{
    public class GameHelper
    {
        #region Singleton
        private static GameHelper instance = null;
        private static readonly object padlock = new object();

        public static GameHelper Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            instance = new GameHelper();
                        }
                    }
                }
                return instance;
            }
        }
        #endregion

        SeleniumHelper browser = SeleniumHelper.Instance;
        OpenCVHelper opencv = OpenCVHelper.Instance;

        public GameHelper()
        {
            browser.OpenFirefox();
            browser.NavigateTo("https://bigfarm.goodgamestudios.com/");
            browser.SwitchTo("game");
        }

        public void Login(string login, string password)
        {
            UpdateScreenshot();

            var loginButton = opencv.FindPattern("Images/Login/LoginButton.png").FirstOrDefault();
            browser.Click(loginButton);

            var loginInput = opencv.FindPattern("Images/Login/LoginInput.png").FirstOrDefault();
            browser.Click(loginInput);

            browser.Keyboard(login);

            var passwordInput = opencv.FindPattern("Images/Login/PasswordInput.png").FirstOrDefault();
            browser.Click(passwordInput);

            browser.Keyboard(password);

            var playButton = opencv.FindPattern("Images/Login/PlayButton.png").FirstOrDefault();
            browser.Click(playButton);
        }

        public void Harvest()
        {
            foreach (string path in Directory.GetFiles("Images/Harvest"))
            {
                UpdateScreenshot();
                var buttons = opencv.FindPattern(path);

                foreach (var button in buttons)
                {
                    browser.Click(button);
                    Thread.Sleep(1000);
                    TrySell();
                }
            }
        }

        public void TrySell()
        {
            UpdateScreenshot();
            var button = opencv.FindPattern("Images/Special/Sell.png").FirstOrDefault();

            if (button != null)
            {
                browser.Click(button);
            }
        }

        public void Dummy()
        {
            string path = browser.TakeScreenshot();
            opencv.LoadScreenshot(path);

            var loginInput = opencv.FindPattern("Images/Login/LoginInput.png").FirstOrDefault();
            browser.Click(loginInput);
        }

        public void Close()
        {
            browser.Close();
        }


        private void UpdateScreenshot()
        {
            string path = browser.TakeScreenshot();
            opencv.LoadScreenshot(path);
        }
    }
}

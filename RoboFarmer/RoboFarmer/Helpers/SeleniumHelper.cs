﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RoboFarmer.Helpers
{
    public class SeleniumHelper
    {
        #region Singleton
        private static SeleniumHelper instance = null;
        private static readonly object padlock = new object();

        public static SeleniumHelper Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            instance = new SeleniumHelper();
                        }
                    }
                }
                return instance;
            }
        }
        #endregion

        private IWebDriver driver;

        public void OpenFirefox()
        {
            if (driver == null)
            {
                driver = new FirefoxDriver();
            }
        }

        public void NavigateTo(string path)
        {
            if (driver != null)
            {
                driver.Navigate().GoToUrl(path);
            }
        }

        public void SwitchTo(string id)
        {
            if (driver != null)
            {
                driver.SwitchTo().Frame(id);
            }
        }

        public void Close()
        {
            if (driver != null)
            {
                driver.Quit();
            }
        }

        public string TakeScreenshot()
        {
            if (driver != null)
            {
                Screenshot scr = ((ITakesScreenshot)driver).GetScreenshot();

                if (scr != null)
                {
                    scr.SaveAsFile("screenshot.png", ScreenshotImageFormat.Png);
                }
            }

            return "screenshot.png";
        }

        public void Click(Point point)
        {
            if (driver != null)
            {
                var canvas = driver.FindElement(By.Id("RanchUI"));

                var html = driver.FindElement(By.TagName("html"));
                //new Actions(driver).MoveToElement(element, point.X, point.Y).Click().Perform();
                new Actions(driver).MoveToElement(html, point.X, point.Y).Perform();
                Thread.Sleep(200);
                new Actions(driver).Click().Perform();
            }
        }

        public void Keyboard(string text)
        {
            if (driver != null)
            {
                new Actions(driver).SendKeys(text).Perform();
            }
        }

    }
}

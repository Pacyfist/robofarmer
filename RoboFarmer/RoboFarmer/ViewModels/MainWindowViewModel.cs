﻿using Caliburn.Micro;
using RoboFarmer.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoboFarmer.ViewModels
{
    public class MainWindowViewModel : PropertyChangedBase
    {
        private IWindowManager _windowManager;

        public MainWindowViewModel(IWindowManager windowManager)
        {
            _windowManager = windowManager;
        }

        GameHelper game = GameHelper.Instance;

        public void OnClose()
        {
            game.Close();
        }

        public void TakeScreenshot()
        {
            SeleniumHelper.Instance.TakeScreenshot();
        }

        public void Login()
        {
            game.Login("eich", "sk8isgr8");
        }

        public void Harvest()
        {
            game.Harvest();
        }

        public void Dummy()
        {
            game.Dummy();
        }
    }
}

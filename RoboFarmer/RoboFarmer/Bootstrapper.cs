﻿using Caliburn.Micro;
using RoboFarmer.ViewModels;
using System.Windows;

namespace RoboFarmer
{
    public class Bootstrapper : BootstrapperBase
    {
        private IWindowManager _windowManager = new WindowManager();

        public Bootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            this._windowManager.ShowWindow(new MainWindowViewModel(this._windowManager));
        }
    }
}
